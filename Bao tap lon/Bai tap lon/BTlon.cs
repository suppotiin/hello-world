﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Bai_tap_lon
{
    class BTlon
    {
        struct ClassForm
        {
            public int id;
            public string name;
            public string phong;
            public int day, month, year,tietBD,tietKT;
        }

        // Tra ve true neu year la nam nhuan
        static bool leapYear(int year)
        {
            return (((year % 4) == 0) && ((year % 100) != 0))
                    || ((year % 400) == 0);
        }

        // Tra ve so ngay cua mot thang cho truoc
        static int lastDayOfMonth(int month, int year)
        {
            switch (month)
            {
                case 2:
                    if (leapYear(year))
                        return 29;
                    else
                        return 28;
                case 4:
                case 6:
                case 9:
                case 11: return 30;
                default: return 31;
            }
        }

        static int numberOfSunday(int month, int year)
        {
            int a = absoluteDay(1, month, year);
            int b = absoluteDay(lastDayOfMonth(month, year), month, year);
            return (b - a - (b % 7 + 1) + 8) / 7;
        }

        // Tinh so ngay trong nam cho den
        // thang/ngay/nam nhap vao
        static int dayNumber(int month, int day, int year)
        {
            // gia su cac thang deu 31 ngay
            int number = (month - 1) * 31 + day;

            // hieu chinh cac thang sau thang hai
            if (month > 2)
            {
                number = number - ((4 * month + 23) / 10);
                if (leapYear(year))
                    number = number + 1;
            }

            return number;
        }

        static int absoluteDay(int day,int month,int year)
        {
            return dayNumber(month, day, year) // so ngay trong nam
                    + 365 * (year - 1)          // so ngay cua cac nam truoc do
                    + (year - 1) / 4   // nam nhuan Julian
                    - (year - 1) / 100 // nam nhuan chan tram
                    + (year - 1) / 400; // nam nhuan Gregorian
        }
        static void Main(string[] args)
        {
            List<ClassForm> listClassForm = new List<ClassForm> ();
            
            int readNumber;
            Console.WriteLine("CHUONG TRINH QUAN LY DON DANG KY MUON PHONG MAY TINH");
            do 
            {
               
                Console.WriteLine("1 - Them don dang ky moi");
                Console.WriteLine("2 - Sua don dang ky muon phong");
                Console.WriteLine("3 - Xoa don dang ky muon phong");
                Console.WriteLine("4 - Danh sach tat ca cac don muon phong");
                Console.WriteLine("5 - Danh sach cac don muon phong theo thoi gian");
                Console.WriteLine("6 - Danh sach cac don muon phong theo nguoi muon va thang");
                Console.WriteLine("7 - Thong ke tan suat su dung");
                Console.WriteLine("8 - Doc du lieu tu file");
                Console.WriteLine("9 - Ghi du lieu ra file");
                Console.WriteLine("10 - Thoat");
                Console.WriteLine("Ban chon so may : _");

                readNumber =int.Parse(Console.ReadLine());

                switch (readNumber)
                {
                    case 1: addForm(ref listClassForm); break;
                    case 2: editForm(ref listClassForm); break;
                    case 3: removeForm(ref listClassForm); break;
                    case 4: printAllForm(listClassForm); break;
                    case 5: printFormInTime(listClassForm); break;
                    case 6: printFormByName(listClassForm); break;
                    case 7: getFrequency(listClassForm); break;
                    case 8: getFromFile(ref listClassForm); break;
                    case 9: writeToFile(ref listClassForm); break;
                    case 10: return; 
                }


            } while (readNumber!=10);
        }

        static bool checkForm(ClassForm form,List<ClassForm> listClassForm)
        {
            int a=form.tietBD,b=form.tietKT;
            foreach (ClassForm i in listClassForm)
            {
                int c=i.tietBD,d=i.tietKT;
                if ((absoluteDay(i.day,i.month,i.year)==absoluteDay(form.day,form.month,form.year)) 
                 && (i.phong==form.phong)
                 && ((a>=c && a<=d)|| (b>=c && b<=d)))
                    return false;
            }
            return true;
        }
        static void getForm(out ClassForm form,List<ClassForm> listClassForm)
        {
            ClassForm newForm;
            newForm.id = 0;
            Console.Write("Nhap ten: ");
            newForm.name = Console.ReadLine();
            Console.Write("Nhap phong: ");
            newForm.phong = Console.ReadLine();
            do {
                
            Console.Write("Nhap ngay: ");
            newForm.day = int.Parse(Console.ReadLine());
            Console.Write("Nhap thang: ");
            newForm.month = int.Parse(Console.ReadLine());
            Console.Write("Nhap nam: ");
            newForm.year = int.Parse(Console.ReadLine());
            Console.WriteLine("Luu y: Neu ngay thang nam ban nhap vao khong dung thi chuong trinh se bat ban nhap lai");
            } while (checkTrueDay(newForm.day,newForm.month,newForm.year)==false);
            Console.Write("Nhap tiet bat dau: ");
            newForm.tietBD = int.Parse(Console.ReadLine());
            Console.Write("Nhap tiet ket thuc: ");
            newForm.tietKT = int.Parse(Console.ReadLine());
            bool isTrueForm = checkForm(newForm, listClassForm);
            //Neu co thi thong bao trung du lieu voi cac don hien co
            while (isTrueForm == false)
            {
                Console.WriteLine("Don nay da bi trung du lieu voi cac don hien co \n Ban co muon nhap lai khong? (co/khong)");

                //Chuyen den phan hoi nguoi su dung co muon nhap tiep don moi hay khong
                string line;
                line = Console.ReadLine();
                if (line == "co") getForm(out form,listClassForm); else break;
                isTrueForm = checkForm(form, listClassForm);
            }
            form = newForm;
        }

        static int getID(ClassForm form,List<ClassForm> listClassForm)
        {
            int maxID=0;
            foreach (ClassForm i in listClassForm)
                if (maxID < i.id) maxID = i.id;
            return maxID+1;
        }

        static void PrintForm(ClassForm form)
        {
            Console.WriteLine("Ma Don la: {0}",form.id);
            Console.WriteLine("Ten nguoi muon la: {0}", form.name);
            Console.WriteLine("Phong muon: {0}",form.phong);
            Console.WriteLine("Ngay/Thang/Nam muon: {0}/{1}/{2}",form.day,form.month,form.year);
            Console.WriteLine("Tiet muon: {0} - {1} \n",form.tietBD,form.tietKT);
        }
        static void addForm(ref List<ClassForm> listClassForm)
        {
            Console.Write("Ban muon nhap bao nhieu don? _");
            int nForm = int.Parse(Console.ReadLine());
            for (int i = 0; i < nForm; i++)
            {
                Console.WriteLine("Don thu {0}", i + 1);
                ClassForm form;
                getForm(out  form,listClassForm);
                //Kiem tra xem ngay muon,tiet muon,phong muon co bi trung hay khong
              
                if (checkForm(form,listClassForm))
                {
                //Neu khong thi:
                
                    //Sinh tu dong ma don moi khong trung voi cac ma don da co
                    //Gan ma don moi cho bien thanh phan "ma"
                    form.id = getID(form,listClassForm);
               
                //Gan bien form vao listClassForm
                listClassForm.Add(form);
                Console.WriteLine("\n Sau day la don ma ban da nhap: \n");
                    PrintForm(form);
                }
            }
        }
        static int findForm(int id,List<ClassForm> listClassForm)
        {
            int dem = 0;
            foreach (ClassForm i in listClassForm)
                if (i.id == id) return dem; else dem++;
            return -1;
        }
        static void editForm(ref List<ClassForm> listClassForm)
        {
            int vt=-1;
            while (true)
            {
                //Yeu cau nguoi su dung nhap ma don
                Console.WriteLine("Nhap ma don muon sua: ");
                int id = int.Parse(Console.ReadLine());
                //lay vi tri cua don muon phong trong danh sach
                vt = findForm(id, listClassForm);
                //neu ket qua la -1  thi thong bao khong co don voi ma muon tim, yeu cau nhap lai neu muon
                //neu co nhap lai thi quay lai buoc 1 trong thuat toan nay
                //neu khong thi thoat khoi thuat toan
                if (vt != -1) break;
                Console.WriteLine("Khong co don voi ma muon tim");
                Console.WriteLine("Ban co muon nhap lai khong?:(co/khong) ");
                string line = Console.ReadLine();
                if (line=="khong")  break;
            }
            if (vt!=-1)
            {
            //neu ket qua khac -1 
              // In thong tin cua don muon phong hien co ra man hinh
                Console.WriteLine("Don muon phong hien co la: ");
                PrintForm(listClassForm[vt]);
              // Yeu cau nguoi su dung nhap du lieu moi
              // Gan du lieu moi vao
                changeForm(ref listClassForm,vt);
                Console.WriteLine("Don da sua: ");
                PrintForm(listClassForm[vt]);
            }
        }

        static void changeForm(ref List<ClassForm> listClassForm,int vt)
        {
            Console.WriteLine("Nhap lai don ban muon sua: ");
            ClassForm form;
            getForm(out form, listClassForm);
            int saveID = listClassForm[vt].id;
            form.id = saveID;
            listClassForm[vt] = form;     
        }

        static void removeForm(ref List<ClassForm> listClassForm)
        {
            int vt=-1;
            while (true)
            {
                //Yeu cau nguoi su dung nhap ma don
                Console.WriteLine("Nhap ma don muon xoa: ");
                int id = int.Parse(Console.ReadLine());
                //lay vi tri cua don muon phong trong danh sach
                vt = findForm(id, listClassForm);
                //neu ket qua la -1  thi thong bao khong co don voi ma muon tim, yeu cau nhap lai neu muon
                //neu co nhap lai thi quay lai buoc 1 trong thuat toan nay
                //neu khong thi thoat khoi thuat toan
                if (vt != -1) break;
                Console.WriteLine("Khong co don voi ma muon tim");
                Console.WriteLine("Ban co muon nhap lai khong?:(co/khong) ");
                string line = Console.ReadLine();
                if (line=="khong")  break;
            }
            if (vt != -1)
            {
                //neu ket qua khac -1 
                // In thong tin cua don muon phong hien co ra man hinh
                Console.WriteLine("Don muon phong hien co la: ");
                PrintForm(listClassForm[vt]);
                // Yeu cau nguoi su dung nhap du lieu moi
                // Gan du lieu moi vao
                Console.WriteLine("Ban co muon xoa khong?:(co/khong) ");
                string line = Console.ReadLine();
                if (line == "khong") return;
                else
                    listClassForm.Remove(listClassForm[vt]);
            }
        }

        static void printAllForm(List<ClassForm> listClassForm)
        {
            Console.WriteLine("Tat ca cac don hien co la: ");
            int dem = 0;
            foreach (ClassForm i in listClassForm)
            {
                dem++;
                Console.WriteLine("Don thu {0}",dem);
                PrintForm(i);
                Console.WriteLine();
            }
        }

        static void printFormInTime(List<ClassForm> listClassForm)
        {
            Console.WriteLine("Nhap vao khoang thoi gian can in danh sach");
            Console.WriteLine("Thoi gian bat dau: ");
            Console.WriteLine("Ngay bat dau: ");
            int bDay = int.Parse(Console.ReadLine());
            Console.WriteLine("Thang bat dau: ");
            int bMonth = int.Parse(Console.ReadLine());
            Console.WriteLine("Nam bat dau: ");
            int bYear = int.Parse(Console.ReadLine());
            Console.WriteLine("\n Thoi gian ket thuc: ");
            Console.WriteLine("Ngay ket thuc: ");
            int eDay = int.Parse(Console.ReadLine());
            Console.WriteLine("Thang ket thuc: ");
            int eMonth = int.Parse(Console.ReadLine());
            Console.WriteLine("Nam ket thuc: ");
            int eYear = int.Parse(Console.ReadLine());

            Console.WriteLine("\n Cac don nam trong khoang thoi gian tren la: ");
            foreach (ClassForm i in listClassForm)
            {
                int aDay=absoluteDay(i.day,i.month,i.year);
                if ((aDay >= absoluteDay(bDay, bMonth, bYear)) && (aDay <= absoluteDay(eDay, eMonth, eYear)))
                    PrintForm(i);
            }
        }

        static void printFormByName(List<ClassForm> listClassForm)
        {
            Console.WriteLine("\n Nhap ten nguoi muon: ");
            string name = Console.ReadLine();
            Console.WriteLine(" Nhap thang: ");
            int month = int.Parse(Console.ReadLine());
            Console.WriteLine("Trong thang {0} {1} da dang ky cac don la: ", month, name);
            foreach (ClassForm i in listClassForm)
            {
                
                if ((i.month == month) && (i.name == name)) PrintForm(i);
                //if (i.name == name) Console.WriteLine("bang");
            }
        }

        

        static void getFrequency(List<ClassForm> listClassForm)
        {
            Console.WriteLine("Nhap thang can thong ke: ");
            int month=int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap nam can thong ke: ");
            int year=int.Parse(Console.ReadLine());
            int phong1=0,phong2=0,phong3=0,phong4 = 0;
            foreach (ClassForm i in listClassForm)
                if (i.month==month)
            {
                if (i.phong == "A5.1") phong1 += i.tietKT - i.tietBD + 1;
                if (i.phong == "A5.2") phong2 += i.tietKT - i.tietBD + 1;
                if (i.phong == "B5.2") phong3 += i.tietKT - i.tietBD + 1;
                if (i.phong == "C4.1") phong4 += i.tietKT - i.tietBD + 1;
            }

            Console.WriteLine("So tiet su dung cua moi phong trong thang: ");
            Console.WriteLine("Phong A5.1: {0} \n Phong A5.2: {1} \n Phong B5.2: {2} \n Phong C4.1: {3} \n", phong1, phong2, phong3, phong4);

            int soNgayCTHoc = lastDayOfMonth(month, year) - numberOfSunday(month, year);
            int sotietCTHoc = soNgayCTHoc * 10;
            Console.WriteLine("Phan tram su dung cua moi phong trong thang: ");
            Console.WriteLine("Phong A5.1: {0} %", (double) phong1 / sotietCTHoc * 100);
            Console.WriteLine("Phong A5.2: {0} %", (double) phong2 / sotietCTHoc * 100);
            Console.WriteLine("Phong B5.2: {0} %", (double) phong3 / sotietCTHoc * 100);
            Console.WriteLine("Phong C4.1: {0} %", (double) phong4 / sotietCTHoc * 100);
        }

        static void getFromFile(ref List<ClassForm> listClassForm)
        {
            Console.WriteLine("Nhap duong dan den file: ");
            string path=Console.ReadLine();
            StreamReader reader = new StreamReader(path);
            if (reader == null) return;
            string input = null;
            while ((input = reader.ReadLine()) != null)
            {
                char[] delims = {',','/','-' };
                string[] strs = input.Split(delims,StringSplitOptions.RemoveEmptyEntries);
                ClassForm form;
                form.id = int.Parse(strs[0]);
                form.day = int.Parse(strs[1]);
                form.month = int.Parse(strs[2]);
                form.year = int.Parse(strs[3]);
                form.tietBD = int.Parse(strs[4]);
                form.tietKT = int.Parse(strs[5]);
                form.name = strs[6];
                form.phong = strs[7];
                listClassForm.Add(form);
            }
            Console.WriteLine("Da nhap thanh cong");
            reader.Close();
        }

        static void writeToFile(ref List<ClassForm> listClassForm)
        {
            Console.WriteLine("Nhap duong dan den file: ");
            string path = Console.ReadLine();
            StreamWriter writer = new StreamWriter(path);
            foreach (ClassForm i in listClassForm)
            {
                writer.WriteLine("{0},{1}/{2}/{3},{4}-{5},{6},{7}",i.id,i.day,i.month,i.year,i.tietBD,i.tietKT,i.name,i.phong);
            }
            Console.WriteLine("Đa xuat ra file thanh cong, vui long mo file {0} de kiem tra",path);
            writer.Close();
        }
        static bool checkTrueDay(int day, int month, int year)
        {
            if (year < 0) return false;
            if (month < 1 || month > 12) return false;
            if (day < 1 || day > lastDayOfMonth(month, year)) return false;
            return true;
        }

    }
}
